# What is it ?

A simple Ceylon webserver based on netty

# Todo

* Handle all HTTP requests type
    * add the netty code for getting a post body
* Provide a default server definition for the most common setup (request logging, errors 404 & 500 managing, ...)
* YAML Config file as an optional way to configure the server
    * port
    * logging
* Plugin support, at least to register new types of views
* Logging: request + app