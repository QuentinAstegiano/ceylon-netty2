import ceylon.html {
    Html,
    Body,
    H1,
    Ul,
    Li
}
import netty {
    ...
}
shared class Saying(shared Integer id, shared String text, shared Map<Integer, String> blop = map { 1 -> "toto", 3 -> "ttata"}) {}

shared void run() {
    HttpServer {
        port = 8090;
        Server {
            Get {
                path = "/sayings";
                (Request req) => if (exists sayings = req.uriParams.get("saying"))
                    then sayings.map((String saying) => Saying(0, saying)).sequence()
                    else [ Saying(0, "default saying") ];
            },
            Get {
                path = "/saying";
                (Request req) => if (exists sayings = req.uriParams.get("saying"))
                    then Saying(1, sayings.fold("")((String a, String b) => a + " " + b))
                    else Saying(0, "default saying");
            },
            Get {
                path = "/sayings";
                (Request req) => if (exists sayings = req.uriParams.get("saying"))
                    then sayings.map((String saying) => Saying(1, saying))
                    else [ Saying(0, "default saying") ];
            },
            Get {
                path = "/html";
                (Request req) => HtmlView(Html {
                    Body {
                        H1 { "Hello Html !"}
                    }
                });
            },
            Get {
                path = "/html_partial";
                (Request req) => HtmlView(Ul {
                    Li { "Element 1"},
                    Li { "Element 2"},
                    Li { "Element 3"}
                });
            },
            Error404 {}
        };
    }.startServer();
}