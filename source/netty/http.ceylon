import ceylon.interop.java {
    javaClass,
    CeylonSet,
    CeylonList
}
import ceylon.regex {
    regex
}

import io.netty.bootstrap {
    ServerBootstrap
}
import io.netty.channel {
    ChannelInboundHandlerAdapter,
    ChannelHandlerContext,
    EventLoopGroup,
    ChannelInitializer,
    ChannelFuture
}
import io.netty.channel.nio {
    NioEventLoopGroup
}
import io.netty.channel.socket {
    SocketChannel
}
import io.netty.channel.socket.nio {
    NioServerSocketChannel
}
import io.netty.handler.codec.http {
    HttpRequestDecoder,
    HttpRequest,
    HttpResponseEncoder,
    QueryStringDecoder
}
import io.netty.handler.logging {
    LoggingHandler,
    LogLevel
}

import java.lang {
    JString=String
}
import java.net {
    InetSocketAddress
}

shared class Server(shared {Handler*} handlers) {
    shared RootHandler root = RootHandler(handlers);
}

shared class RootHandler({Handler*} childs) {

    shared void process(Request req, Response resp) {
        if (exists handler = childs
            .filter((Handler handler) => handler.isApplyableTo(req, resp))
            .first) {
            handler.handle(req, resp);
        } else {
            resp.write(ErrorView("No route found ?!", 500));
        }
    }
}

"Base handler node for the server configuration"
shared abstract class Handler({Handler*} childs) {

    "The action performed by this handler"
    shared formal Response act(Request req, Response resp);

    "Test if the handler should be applyied to this request.
     Note that the handlers are hierarchical: if a handler is
     not applyable to a request, then it's children node will
     not be tested."
    shared formal Boolean isApplyableTo(Request req, Response resp);

    "If applyable, perform the handler action to this request
     and pass it to its child"
    shared Response handle(Request req, Response resp) {
        if (isApplyableTo(req, resp)) {
            value thisResp = act(req, resp);
            return if (exists child = childs
                .filter((Handler handler) => handler.isApplyableTo(req, resp))
                .first) then child.handle(req, thisResp) else thisResp;
        } else {
            return resp;
        }
    }
}

shared class Filter(Response filter(Request req, Response resp), {Handler*} childs, String applyTo = ".*")
extends Handler(childs){
    act(Request req, Response resp) => filter(req, resp);
    isApplyableTo(Request req, Response resp) => regex(applyTo).test(req.uri);
}

shared abstract class EndPoint()
extends Handler({}) {

    shared formal Object|String|View execute(Request req);

    shared actual Response act(Request req, Response resp) {
        value data = execute(req);
        if (is String data) {
            resp.write(PlainTextView(data));
        } else if (is View data) {
            resp.write(data);
        } else {
            resp.write(JsonView(data));
        }
        return resp;
    }
}

shared class Get(String path, Object|String get(Request req))
extends EndPoint() {
    execute(Request req) => get(req);
    isApplyableTo(Request req, Response resp) => req.method == "GET" && req.uri == path;
}

shared class Error404()
extends EndPoint() {
    execute(Request req) => ErrorView("Page not found", 404);
    isApplyableTo(Request req, Response resp) => true;
}

shared class Request(HttpRequest req) {

    String toCeylonString(JString s) => s.string;

    shared Map<String, <String|Integer>[]> headers = CeylonSet(req.headers().names())
        .map(toCeylonString)
        .tabulate((String header) => CeylonList(req.headers().getAll(header))
                                        .map((JString val) =>
                                                if (is Integer intVal = Integer.parse(val.string)) then intVal
                                                else val.string)
                                        .sequence());

    shared String fullPath = req.uri();

    value queryString = QueryStringDecoder(req.uri());
    shared String uri = queryString.path();

    value parameters = queryString.parameters();
    shared Map<String, String[]> uriParams = CeylonSet(parameters.keySet())
        .map(toCeylonString)
        .tabulate((String param) {
            if (exists list = parameters.get(JString(param))) {
                return CeylonList(list).map(toCeylonString).sequence();
            } else {
                return null;
            }
        }).coalescedMap;

    shared String method = req.method().name();

    shared String protocolVersion = req.protocolVersion().string;
}

void requestLog(ChannelHandlerContext ctx, Request req) {
    String ip = if (is InetSocketAddress socketAddress = ctx.channel().remoteAddress())
        then socketAddress.address.hostAddress
        else "unknown";

    print("127.0.0.1 - ``ip`` [] \"``req.method`` ``req.fullPath`` ``req.protocolVersion``\" 200 size");
}

class HttpServerHandler(Server server) extends ChannelInboundHandlerAdapter() {

    shared actual void channelReadComplete(ChannelHandlerContext? ctx) {
        if (exists ctx) {
            ctx.flush();
        }
    }

    shared actual void channelRead(ChannelHandlerContext? ctx, Object? msg) {
        if (exists ctx, is HttpRequest msg) {
            server.root.process(Request(msg), Response(ctx));
        }
    }

    shared actual void exceptionCaught(ChannelHandlerContext? ctx, Throwable? cause) {
        if (exists cause, exists ctx) {
            cause.printStackTrace();
            ctx.close();
        }
    }
}

shared class HttpServer(Server server, Integer port = 8080) {

    class ChildHandler() extends ChannelInitializer<SocketChannel>() {
        shared actual void initChannel(SocketChannel? ch) => ch ?. pipeline()
            ?. addLast(HttpRequestDecoder())
            ?. addLast(HttpResponseEncoder())
            ?. addLast(HttpServerHandler(server));
    }

    variable ChannelFuture? channelFuture = null;

    shared void startServer() {
        EventLoopGroup bossGroup = NioEventLoopGroup();
        EventLoopGroup workerGroup = NioEventLoopGroup();

        try {
            ServerBootstrap b = ServerBootstrap();
            b.group(bossGroup, workerGroup)
                .channel(javaClass<NioServerSocketChannel>())
                .handler(LoggingHandler(LogLevel.info))
                .childHandler(ChildHandler());

            ChannelFuture f = b.bind(port).sync();
            print("Http server started on port ``port``");
            channelFuture = f;

            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    shared Boolean isStarted() =>
        if (exists started = channelFuture?.channel()?.open, started) then true
        else false;

    shared void stopServer() {
        channelFuture?.channel()?.close();
        channelFuture?.channel()?.parent()?.close();
    }
}