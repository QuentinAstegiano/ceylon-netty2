"Default documentation for module `netty`."
native ("jvm")
module netty "1.0.0" {
    import ceylon.json "1.3.1";
    import ceylon.interop.java "1.3.1";
    import ceylon.regex "1.3.1";
    import ceylon.test "1.3.1";
    shared import ceylon.html "1.3.1";
    shared import maven : "io.netty:netty-all" "4.1.6.Final";
    shared import maven : "com.fasterxml.jackson.core:jackson-core" "2.8.6";
    shared import maven : "com.fasterxml.jackson.core:jackson-databind" "2.8.6";
    shared import maven : "org.yaml:snakeyaml" "1.17";

}
