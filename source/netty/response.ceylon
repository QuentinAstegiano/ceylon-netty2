import ceylon.html {
    Element
}
import ceylon.interop.java {
    javaClass
}
import ceylon.json {
    JsonArray
}
import ceylon.test {
    test
}

import com.fasterxml.jackson.core {
    JsonGenerator
}
import com.fasterxml.jackson.databind {
    ObjectMapper,
    SerializerProvider,
    JsonSerializer
}
import com.fasterxml.jackson.databind.\imodule {
    SimpleModule
}
import com.fasterxml.jackson.databind.ser.std {
    StdSerializer
}

import io.netty.buffer {
    Unpooled
}
import io.netty.channel {
    ChannelHandlerContext
}
import io.netty.handler.codec.http {
    HttpVersion,
    HttpResponseStatus,
    HttpHeaderNames,
    DefaultFullHttpResponse,
    HttpHeaderValues
}
import io.netty.util {
    CharsetUtil
}

import java.lang.reflect {
    Type
}

shared interface View {
    shared formal Integer httpStatus();
    shared formal Object data();
    shared formal {<String->String>*} headers();
}

object json {
    shared ObjectMapper mapper = ObjectMapper();
    value mdl = SimpleModule();
    mdl.addSerializer(object extends StdSerializer<Iterable<Object>>(javaClass<Iterable<Object>>()) {

        shared actual void serialize(Iterable<Object>? list, JsonGenerator? gen, SerializerProvider? provider) {
            if (exists list, exists gen, exists provider) {
                gen.writeStartArray();
                list.each((Object element) =>
                    if (is String element) then gen.writeString(element)
                    else if (is Integer element) then gen.writeNumber(element)
                    else if (is Boolean element) then gen.writeBoolean(element)
                    else gen.writeObject(element));
                gen.writeEndArray();
            }
        }

    });
    mdl.addSerializer(object extends StdSerializer<Map<Object, Object>>(javaClass<Map<Object, Object>>()) {

        shared actual void serialize(Map<Object,Object>? map, JsonGenerator? gen, SerializerProvider? provider) {
            if (exists map, exists gen, exists provider) {
                gen.writeStartObject();
                map.each((Object key -> Object val) =>
                    if (is String val) then gen.writeStringField(key.string, val)
                    else if (is Integer val) then gen.writeNumberField(key.string, val)
                    else if (is Boolean val) then gen.writeBooleanField(key.string, val)
                    else gen.writeObjectField(key.string, val));
                gen.writeEndObject();
            }
        }

    });
    mapper.registerModule(mdl);
}

shared class JsonView(Object bean)
        satisfies View {
    shared default actual Integer httpStatus() => 200;
    shared default actual {<String->String>*} headers() =>
            { "content-type"->"application/json; charset=UTF-8" };
    data() => json.mapper.writeValueAsString(bean);
}

shared class PlainTextView(String message)
        satisfies View {
    shared default actual Integer httpStatus() => 200;
    shared default actual {<String->String>*} headers() =>
            { "content-type"->"text/plain; charset=UTF-8" };
    data() => message;
}

shared class ErrorView(String message, Integer status)
        extends PlainTextView(message) {
    httpStatus() => status;
}

shared class HtmlView(Element html)
        satisfies View {
    shared default actual Integer httpStatus() => 200;
    shared default actual {<String->String>*} headers() =>
            { "content-type"->"text/html; charset=UTF-8" };
    data() => html.string;
}

shared class Response(ChannelHandlerContext context) {

    shared void write(View view) {
        value response = DefaultFullHttpResponse(HttpVersion.http11,
            HttpResponseStatus.valueOf(view.httpStatus()),
            Unpooled.copiedBuffer(view.data().string, CharsetUtil.utf8));

        view.headers().each((String name->String val) => response.headers().set(name, val));

        value readableBytes = response.content().readableBytes();
        response.headers().setInt(HttpHeaderNames.contentLength, readableBytes);
        response.headers().set(HttpHeaderNames.connection, HttpHeaderValues.keepAlive);
        context.write(response);
    }
}
