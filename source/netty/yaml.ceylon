import ceylon.collection {
    HashMap,
    ArrayList
}
import ceylon.language.meta.declaration {
    FunctionOrValueDeclaration,
    ValueDeclaration,
    OpenClassType,
    OpenClassOrInterfaceType
}
import ceylon.language.meta.model {
    ClassModel
}
import ceylon.language.serialization {
    deserialization
}

import java.util {
    JMap=Map,
    JList=Collection
}

import org.yaml.snakeyaml {
    SnakeYaml=Yaml
}
import ceylon.language.meta {
    classDeclaration
}

class Deserializer<out Instance>(ClassModel<Instance> rootClazz) {
    value dc = deserialization<String>();

    shared Instance hydrate(Map<String,Object> model) => Parser<Instance>(rootClazz).hydrate(model);

    class Parser<out Instance>(ClassModel<Instance> clazz) {

        value idMap = HashMap<ClassModel<>,Integer>();

        String getNextInstanceId<out Inst>(ClassModel<Inst> clazz) {
            value currentCount = idMap.getOrDefault(clazz, 0);
            idMap.put(clazz, currentCount + 1);
            return clazz.string + "_" + currentCount.string;
        }

        String getAttributeId(String instanceId, String|ValueDeclaration param) {
            if (is ValueDeclaration param) {
                return instanceId + "." + param.name;
            } else {
                return instanceId + "." + param.string;
            }
        }

        value id = getNextInstanceId(clazz);
        dc.instance(id, clazz);
        if (exists parameters = clazz.declaration.parameterDeclarations) {
            parameters.each((FunctionOrValueDeclaration param) {
                if (is ValueDeclaration param) {
                    value attrId = getAttributeId(id, param);
                    dc.attribute(id, param, attrId);
                }
            });
        }

        shared Instance hydrate(Map<String,Object> model) {
            model.each((String key->Object val) {
                //dc.instanceValue(getAttributeId(id, key), val);
                if (is Map<String,Object> val) {
                    value param = clazz.declaration.getParameterDeclaration(key);
                    if (is ValueDeclaration param, is OpenClassType type = param.openType) {
                        value childClazz = type.declaration.classApply<>();
                        value childParser = Parser<Anything>(childClazz);
                        value childElt = childParser.hydrate(val);
                        dc.instanceValue(getAttributeId(id, key), childElt);
                    }
                } else if (is List<Object> val) {
                    value param = clazz.declaration.getParameterDeclaration(key);
                    if (is ValueDeclaration param, is OpenClassOrInterfaceType type = param.openType) {

                    }
                    dc.instanceValue(getAttributeId(id, key), val);
                } else {
                    dc.instanceValue(getAttributeId(id, key), val);
                }
            });
            return dc.reconstruct<Instance>(id);
        }
    }
}

class YamlReader() {
//    shared alias ChildType => Map<>|List<>|String;
    Map<String,Object>|List<Object>|String|Null convert(Anything jObject) {
        if (is JMap<out Anything,out Anything> jObject) {
            value cmap = HashMap<String,Object>();
            for (entry in jObject.entrySet()) {
                if (exists key = entry.key, exists val = entry.\ivalue) {
                    if (exists child = convert(val)) {
                        cmap.put(key.string, child);
                    }
                }
            }
            return cmap;
        } else if (is JList<out Anything> jObject) {
            /*
            ArrayList<Map<String, ChildType>>|ArrayList<String>|Null clist;
            for (elt in jObject) {
                if (exists child = convert(elt)) {
                    if (is String child) {
                        clist = ArrayList<String>();
                    } else if (is Map<String, ChildType> child){
                        clist = ArrayList<Map<String, ChildType>>();
                    } else {
                        clist = null;
                    }
                } else {
                    clist = null;
                }
                break;
            } else {
                clist = null;
            }
            */
            value clist = ArrayList<Object>();
            for (elt in jObject) {
                if (exists child = convert(elt)) {
                    clist.add(child);
                }
            }
            return clist;
        } else if (exists jObject) {
            return jObject.string;
        } else {
            return null;
        }
    }

    shared Map<String,Object>|List<Object>|String|Null read(String data) => convert(SnakeYaml().load(data));
}

shared class Yaml() {

    shared Instance? read<Instance>(ClassModel<Instance> clazz, String data) {
        value yaml = YamlReader().read(data);
        return if (is Map<String,Object> yaml) then Deserializer<Instance>(clazz).hydrate(yaml) else null;
    }
}

