"Default documentation for module `test.netty`."
native ("jvm")
module test.netty "1.0.0" {
    import java.base "8";
    import ceylon.interop.java "1.3.1";
    import ceylon.test "1.3.1";

    import maven : "org.apache.httpcomponents:httpclient" "4.5.2";
    import maven : "org.apache.httpcomponents:httpcore" "4.4.4";

    import netty "1.0.0";
}
