import ceylon.test {
    test,
    assertTrue,
    assertEquals
}

import java.io {
    BufferedReader,
    InputStreamReader
}
import java.lang {
    Thread
}

import org.apache.http {
    HttpResponse
}
import org.apache.http.client {
    HttpClient
}
import org.apache.http.client.methods {
    HttpGet
}
import org.apache.http.impl.client {
    DefaultHttpClient
}
import netty {
    HttpServer,
    Get,
    Request,
    Server,
    Error404,
    Filter
}

shared class Saying(shared Integer id, shared String text) {
}

class ServerThread() extends Thread() {
    shared HttpServer http = HttpServer(Server {
        Filter {
            filter = (req, resp) {
                print("Req: " + req.fullPath);
                return resp;
            };
            Get {
                path = "/test";
                (Request req) {
                    print("hello from " + req.uri + " with params " + req.uriParams.string);
                    return "hello";
                };
            },
            Get {
                path = "/saying";
                (Request req) => if (exists sayings = req.uriParams.get("saying"))
                    then Saying(1, sayings.fold("")((String a, String b) => a + " " + b))
                    else Saying(0, "default saying");
            },
            Error404 {}
        }
    });
    shared actual void run() => http.startServer();
}

HttpClient client = DefaultHttpClient();

String getResponseContent(HttpGet get) {
    HttpResponse response = client.execute(get);

    variable String content = "";
    try (input = response.entity.content) {
        value reader = BufferedReader(InputStreamReader(input));
        while (exists line = reader.readLine()) {
            content += line;
        }
    }
    return content;
}

test
shared void startServer() {
    value server = ServerThread();
    server.start();

    variable Integer attempts = 0;
    while (!server.http.isStarted()&& attempts<50) {
        Thread.sleep(50);
        attempts ++;
    }

    assertTrue(server.http.isStarted(), "Server not started after ``50 * 50`` ms");

    assertEquals(getResponseContent(HttpGet("http://localhost:8080/test")), "hello");
    assertEquals(getResponseContent(HttpGet("http://localhost:8080/test?var1=bob&var2=john&var1=bob_again")), "hello");
    assertEquals(getResponseContent(HttpGet("http://localhost:8080/nope")), "Page not found");

    print("Saying: " + getResponseContent(HttpGet("http://localhost:8080/saying")));
    print("Saying 2: " + getResponseContent(HttpGet("http://localhost:8080/saying?saying=hello&saying=world")));

    server.http.stopServer();
    while (server.http.isStarted()) {
        Thread.sleep(50);
    }
}