import netty {
    Yaml
}
import ceylon.test {
    test,
    assertEquals
}

serializable class Simple(shared String prop) {}

test
shared void should_deserialize_very_simple_object() {
    assert (exists simple = Yaml().read<Simple>(`Simple`, "prop: val1"));
    assertEquals(simple.prop, "val1");
}

serializable class Container(shared Simple simple) {}

test
shared void should_deseriialiaze_nested_objects() {
    assert (exists container = Yaml().read<Container>(`Container`, "simple:\n  prop: val1"));
    assertEquals(container.simple.prop, "val1");
}

serializable class StringListContainer(shared List<String> props) {}

test
shared void should_deserialize_simple_lists() {
    assert (exists list = Yaml().read<StringListContainer>(`StringListContainer`, "props:\n  [\"val1\", \"val2\"]"));
    assertEquals(list.props, [ "val1", "val2" ]);
}

serializable class SimpleListContainer(shared List<Simple> simples)  {}

test
shared void should_deserialize_lists_of_objects() {
    value data = """simples:
                      - prop: val1
                      - prop: val2
                      - prop: val3
                  """;
    value list = Yaml().read<SimpleListContainer>(`SimpleListContainer`, data);
    assert (exists list);
    assertEquals(list.simples, [ Simple("val1"), Simple("val2"), Simple("val3") ]);
}

test
shared void testSnake() {

    serializable class Address(shared String zipCode, shared String city) {
        string => "[" + zipCode + ":" + city + "]";
    }

    serializable class Person(shared String name, shared String lastName, shared Address address) {
        string => "Person: " + name + " " + lastName + " @ " + address.string;
    }

    value msg = "name: John\nlastName: Smith\naddress:\n  zipCode: \"00900\"\n  city: London";
    Person? person = Yaml().read<Person>(`Person`, msg);
    if (exists person) {
        print("Person: " + person.string);
    }
}